import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { CONFIG } from './config';
import { User } from './user';

let apiUrl = CONFIG.apiUrl;

@Injectable()
export class UserService {

    constructor(private http: Http) {}

    getUsers() {
        return this.http.get(apiUrl)
            .map((response: Response) => <User[]>response.json())
            .catch((error: Response) => {
                console.log(error);
                return Observable.throw('error');
            });
    }
}