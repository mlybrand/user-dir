import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UserListComponent } from './user-list.component';
import { UserService } from './user.service';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('UserListComponent', function () {
  let comp: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent ],
      // TODO: need to stub or mock service
      providers: [ UserService ],
      imports: [ FormsModule, HttpModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    comp = fixture.componentInstance;
  });

  it('should create component', () => expect(comp).toBeDefined() );

  it('should have expected search form', () => {
    let de: DebugElement;
    de = fixture.debugElement.query(By.css('#search-form'));
  
    fixture.detectChanges();
    const form = de.nativeElement;
    expect(form.querySelector('input#search-term')).toBeDefined();
    expect(form.querySelector('button#search-button')).toBeDefined();
  });

  // it('should have initial set of users', () => {
  //   console.log('foo');
  // });
});
