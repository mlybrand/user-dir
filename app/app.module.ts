import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { UserListComponent }  from './user-list.component';
import { UserService } from './user.service';

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule ],
  declarations: [ UserListComponent ],
  providers:    [ UserService ],
  bootstrap:    [ UserListComponent ]
})
export class AppModule { }
