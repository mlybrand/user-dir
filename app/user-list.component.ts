import { Component, OnInit } from '@angular/core';

import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'user-list',
  templateUrl: 'app/user-list.component.html',
  styleUrls: [ 'app/user-list.component.css' ]
})
export class UserListComponent implements OnInit  {
  users: User[];
  displayUsers: User[];
  searchTerm: String;
  processedSearchTerm: String;
  showFooter: Boolean = false;

  constructor(private userService: UserService ) { }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(
        users => {
          this.users = users;
          this.displayUsers = users;
        },
        error => console.log(error)
      );
  }

  search() {
    if (this.searchTerm.trim()) {
      this.showFooter = true;
      let s = this.searchTerm.trim().toLocaleLowerCase();
      this.displayUsers = this.users.filter(
        x => 
          x.email.toLocaleLowerCase().includes(s)
          ||
          x.name.toLocaleLowerCase().includes(s));
    } else {
      this.showFooter = false;
      this.displayUsers = this.users;
    }
    this.processedSearchTerm = this.searchTerm;
  }  
}
