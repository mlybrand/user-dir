import { browser, element, by } from 'protractor';

describe('QuickStart E2E Tests', function () {

  beforeEach(function () {
    browser.get('');
  });

  it('should display search form', function () {
    expect(element(by.css('input#search-term'))).toBeDefined();
    expect(element(by.css('button#search-button'))).toBeDefined();
  });

});
