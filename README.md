# Angular QuickStart Source
I started by cloning the [angular.io quickstart](https://angular.io/docs/ts/latest/quickstart.html).

# Bootstrap
I also chose to take advantage of bootstrap for my responsive needs (just making sure the breakpoint was where I needed it) [bootstrap](http://getbootstrap.com).

# Gulp
There was also an issue with the asynchrony around using external templates. I like external templates for code separation, but do like to have inline templates for deployment. So, I added some gulp tasks to handle this inlining of the templates and to pair that with the TypeScript transpilation.  Gulp to the rescue!! [gulp](http://gulpjs.com). However, it is only watching .ts extensions. Given more time I would fine-tool this build process to watch .html and .css files as well.

# Commit History
Assuming you are viewing this project from a .zip file and don't have access to the commit history (and assuming you are interested):

* fdbf27d - Mark Lybrand, 3 minutes ago : finished tweaking CSS for look and feel
* 90758a5 - Mark Lybrand, 3 hours ago : finished implementing stated functional requirements
* ca618ff - Mark Lybrand, 10 hours ago : wired up service to use json endpoint. changed component to use react observable.
* 14c2428 - Mark Lybrand, 11 hours ago : moved user list into a service
* ea95712 - Mark Lybrand, 25 hours ago : moved users into a model and wired up the component to build users based on list of objects based on model
* d761139 - Mark Lybrand, 34 hours ago : added colors for alternating rows to mockup
* 87d9ab3 - Mark Lybrand, 34 hours ago : finished laying out mock content to make sure the responsiveness is as expected.
* 548e96a - Mark Lybrand, 2 days ago : made the assertions more specific to the form elements expected
* f95af68 - Mark Lybrand, 2 days ago : added gulp stuff to do typescript compilation and template inlining.  updated component spec to pass with search form on it
* 27c3ac8 - Mark Lybrand, 2 days ago : added gulp to project. will use to try to circumvent the issue with tests to running with external templates (since I need to inline templates anyway)
* c48510b - Mark Lybrand, 2 days ago : implemented responsive search form
* cc3cf1e - Mark Lybrand, 2 days ago : added bootstrap
* 1de8312 - Mark Lybrand, 2 days ago : moved template to external file. karma tests are broken until I can figure out how to get the compilation to work
* e5554f0 - Mark Lybrand, 2 days ago : a little renaming of the angular 2 starter solution in preparation for implementing solution
* 094c48e - Mark Lybrand, 3 days ago : Initial commit