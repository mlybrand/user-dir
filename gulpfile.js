var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    tsProject = ts.createProject('tsconfig.json'),
    inlineNg2Template = require('gulp-inline-ng2-template');

gulp.task('tsc', function() {
    var tsResult = gulp.src('app/**/*.ts')
        .pipe(inlineNg2Template())
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('app'));
});

gulp.task('tsc-w', ['tsc'], function() {
    gulp.watch('app/**/*.ts', ['tsc']);
});